<?php

namespace Drupal\rufi_versions\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\rufi_versions\VersionsViewBuilderService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'vocab_clone_formatter_type' formatter.
 *
 * @FieldFormatter(
 *   id = "vocab_clone_formatter_type",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "vocab_clone_field_type"
 *   }
 * )
 */
class VocabularyCloneFormatterType extends FormatterBase {


  /**
   * Drupal\rufi_versions\VersionsViewBuilderService definition.
   *
   * @var \Drupal\rufi_versions\VersionsViewBuilderService
   */
  protected VersionsViewBuilderService $rufiVersionsViewbuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->rufiVersionsViewbuilder = $container->get('rufi_versions.viewbuilder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    /** @var \Drupal\node\NodeInterface $node */
    if ($node = $items->getParent()->getEntity()) {
      $elements = $this->rufiVersionsViewbuilder->getRenderedView($node);
    }

    return $elements;
  }

}
