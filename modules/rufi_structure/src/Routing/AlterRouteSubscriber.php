<?php

namespace Drupal\rufi_structure\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Replace login form by an empty controller because .
 */
class AlterRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // Change path '/user/login' to '/rufi/login'.
    // Set empty controller instead of login.form.
    if ($route = $collection->get('user.login')) {
      $route->setPath('/rufi/login');
      $route->addDefaults([
        '_title' => 'RulesFinder login',
      ]);
    }

    // Change path '/user/register' to '/rufi/login'.
    if ($route = $collection->get('user.register')) {
      $route->setPath('/rufi/register');
    }

    // Change path '/user/register' to '/rufi/login'.
    if ($route = $collection->get('user.pass')) {
      $route->setPath('/rufi/password');
    }

    // Change path '/user/logout' to '/rufi/logout'.
    if ($route = $collection->get('user.logout')) {
      $route->setPath('/rufi/logout');
    }

  }

}
