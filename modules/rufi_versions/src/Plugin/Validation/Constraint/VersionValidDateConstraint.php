<?php

namespace Drupal\rufi_versions\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Plugin implementation of the 'version_valid_date_constraint'.
 *
 * @Constraint(
 *   id = "version_valid_date_constraint",
 *   label = @Translation("Version valid date constraint", context = "Validation"),
 * )
 */
class VersionValidDateConstraint extends Constraint {

  /**
   * The message that will be shown if the value is empty.
   *
   * @var string
   */
  public $isEmpty = '%value is empty';

  /**
   * The message that will be shown if the value is not unique.
   *
   * @var string
   */
  public $notInFuture = '%value is not in the future';

}
